function editresult(id,activity) {
    $("#abc").css("display","block");
    $("#fixtures").css("filter","opacity(30%)");
    var activity_id = document.getElementById("activity_id");
    var user_activity = document.getElementById("user_activity");
    activity_id.value = id;
    user_activity.value = activity;
}
function add_activity(activity) {
    var resultDetails = new FormData();
    resultDetails.append("activity",activity);
    $.ajax({
        type:"POST",
        url:"add_activity_func",
        data:resultDetails,
        cache: false,
        contentType: false,
        processData: false,
        success: function(result){
//                alert('please refresh the page to see the updated result');
            alert(result.res);
        }
    });
}
function delete_activity(activity_id) {
    var resultDetails = new FormData();
    resultDetails.append("activity_id",activity_id);
    $.ajax({
        type:"POST",
        url:"delete_activity",
        data:resultDetails,
        cache: false,
        contentType: false,
        processData: false,
        success: function(result){
//                alert('please refresh the page to see the updated result');
            alert(result.res);
            location.reload();
        }
    });
}
$(document).ready(function() {

    $('#headerTable').DataTable({
    "pagingType": "full_numbers"
    });
    $('.dataTables_length').addClass('bs-select');
    $('#update').click(function(){
        var id   = $('#activity_id').val();
        var user_activity   = $('#user_activity').val();
        var resultDetails = new FormData();
        resultDetails.append("id",id);
        resultDetails.append("user_activity",user_activity);
        $.ajax({
            type:"POST",
            url:"update_result",
            data:resultDetails,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result){
//                alert('please refresh the page to see the updated result');
                location.reload();
                $("#abc").css("display","none");
                $("#fixtures").css("filter","opacity(100%)");
                location.reload();
            }
        });
    });

});

