$(document).ready(function() {
    $("#user_login").submit(function(){
        var frm = $('#user_login');
        $.ajax({
            type:"POST",
            url:"user_login",
            data:frm.serialize(),
            success: function (data) {
                var json = jQuery.parseJSON(JSON.stringify(data));
                console.log(json)
                if (json.res == 'admin') {
//                    alert('admin');
                    window.location = "admin_view";
                } else if (json.res == 'user') {
//                    alert('user');
                    window.location = "activity_list";
                } else {
                    alert('incorrect username or password');
                }
            }
        });
    });
});