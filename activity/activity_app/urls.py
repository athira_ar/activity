from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('activity_list', views.activity_list, name='activity_list'),
    path('add_activity', views.add_activity, name='add_activity'),
    path('add_activity_func', views.add_activity_func, name='add_activity_func'),
    # path('update_activity', views.update_activity, name='update_activity'),
    path('user_login', views.user_login, name='user_login'),
    path('registration', views.registration, name='registration'),
    path('register_function', views.register_function, name='register_function'),
    path('admin_view', views.admin_view, name='admin_view'),
    path('update_result', views.update_result, name='update_result'),
    path('delete_activity', views.delete_activity, name='delete_activity'),
]
