from django.http import HttpResponse
from django.shortcuts import render
from django.db import connections

from .backend.config_data import config_json
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
user_name = ''
password = ''

def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    print('here index')
    return render(request, 'activity_app/index.html')


@csrf_exempt
def user_login(request):
    # print('here login')
    if request.method == 'POST':
        global user_name
        global password
        user_name = request.POST.get('uname')
        password = request.POST.get('password')
        # if (user_name=="admin" and password=="admin@123"):
        #     return HttpResponse(json.dumps({'res': "success"}),
        #                         content_type="application/json")
        # else:
        #     return HttpResponse(json.dumps({'res': "error"}),
        #                         content_type="application/json")

        # cursor = connections['default'].cursor()
        # cursor.execute(
        #     "select user_role,user_password,user_type from user WHERE user_name='" + user_name + "'")
        # result = cursor.fetchall()
        # connections['default'].commit()

        # role = result[0][0]
        # role = 'admin'
        # db_pass = result[0][1]
        # db_pass = 'athira'
        if password == "admin":
            return HttpResponse(json.dumps({'res': "admin"}),
                                content_type="application/json")
        elif password == "user":
            return HttpResponse(json.dumps({'res': "user"}),
                                content_type="application/json")
        # if role == "admin" and password == db_pass:
        #     return HttpResponse(json.dumps({'res': "admin"}),
        #                         content_type="application/json")
        # elif role == "user" and password == db_pass:
        #     return HttpResponse(json.dumps({'res': "user"}),
        #                         content_type="application/json")
        else:
            return HttpResponse(json.dumps({'res': "error"}),
                                content_type="application/json")


@csrf_exempt
def admin_view(request):
    # global user_name
    # user_name = 'pravi'
    print('admin view')
    global password
    print(password)
    if password == 'admin':
        # return render(request, 'activity_app/admin_view.html')
        cursor = connections['default'].cursor()
        cursor.execute("select activity_id, user_name, user_type, user_activity from user_activities")
        fieldnames = [name[0] for name in cursor.description]
        result = []
        for row in cursor.fetchall():
            row_set = []
            print(row)
            print("----------------")
            for field in zip(fieldnames, row):
                row_set.append(field)
            result.append(dict(row_set))
        print(result)
        connections['default'].close()
        config_json.update({"teamdata": result})
    else:
        result = []
        config_json.update({"teamdata": result})
    filen = "activity_app/admin_view.html"
    title = "ACTIVITIES"
    return render(request, 'activity_app/admin_base.html',
                  {'home_status': 'active', 'config_json': config_json, 'title': title, 'filen': filen})


@csrf_exempt
def registration(request):
    print('here registration')
    return render(request, 'activity_app/registration.html')


@csrf_exempt
def register_function(request):
    import requests
    print('here registration1')
    username = request.POST.get('book_name')
    user_email = request.POST.get('author')
    user_type = request.POST.get('book_count')
    activity = []
    i = 0
    while i < 10:
        r = requests.get('http://www.boredapi.com/api/activity?type=' + user_type)  # type=education
        # print(r.headers['content-type'])
        y = r.json()

        # y = json.loads(r.json())
        if y['activity'] not in activity:
            activity.append(y['activity'])
            i = i + 1
    print(activity)
    separator = ', '
    activity_list1 = separator.join(activity)
    print(separator.join(activity))
    print(username)
    print(user_email)
    print(user_type)
    user_role = 'user'
    user_password = 'user'
    cursor = connections['default'].cursor()
    print(cursor)
    sql = "insert into user (user_name, user_email, user_type, user_activities, user_role, user_password)" \
          " values(%s, %s, %s, %s, %s, %s) "
    cursor.execute(sql, (username, user_email, user_type, activity_list1, user_role, user_password))
    connections['default'].commit()
    from datetime import date

    today = str(date.today())
    sql = "insert into  activity_details (user_name, date, count,user_type) values(%s, %s, %s, %s) "
    cursor.execute(sql, (username, today, 0, user_type))
    connections['default'].commit()
    for i in activity:
        sql = "insert into  user_activities (user_name, user_type, user_activity) values(%s, %s, %s) "
        cursor.execute(sql, (username, user_type, i))
    from django.core.mail import send_mail
    from django.conf import settings
    try:
        send_mail(
            'Activity - Registration',
            "Hi, Registration Successful. username: " + username + " password: user",
            settings.EMAIL_HOST_USER,
            [user_email],
            fail_silently=False,
            auth_user=settings.EMAIL_HOST_USER,
            auth_password=settings.EMAIL_HOST_PASSWORD,
        )
    except Exception as e:
        print(e)
    val = "Registration Successful. username: " + username + " password: user"
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")


@csrf_exempt
def activity_list(request):
    global user_name
    cursor = connections['default'].cursor()
    cursor.execute(
        "select activity_id, user_name, user_type, user_activity from user_activities WHERE user_name='" + user_name +
        "'")
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        row_set = []
        for field in zip(fieldnames, row):
            row_set.append(field)
        result.append(dict(row_set))
    connections['default'].close()
    config_json.update({"teamdata": result})
    filen = "activity_app/update_activity.html"
    title = "ACTIVITY LIST"
    return render(request, 'activity_app/base.html',
                  {'home_status': 'active', 'config_json': config_json, 'title': title, 'filen': filen})


@csrf_exempt
def add_activity(request):
    global user_name
    print('inside add_activity')
    cursor = connections['default'].cursor()
    cursor.execute("select  user_activity from user_activities WHERE user_name='" + user_name + "'")
    fieldnames = [name[0] for name in cursor.description]
    print(fieldnames)
    result = []
    for row in cursor.fetchall():
        result.append(row[0])
    connections['default'].close()
    import requests
    result1 = []
    i = 0
    while i < 10:
        r = requests.get('http://www.boredapi.com/api/activity?type=social')  # type=education
        y = r.json()
        activity = []
        if y['activity'] not in result:
            activity.append(y['activity'])
            result.append(y['activity'])
            i = i + 1
            fieldnames = ['activity']
            result2 = zip(fieldnames, activity)
            result1.append(dict(result2))
    connections['default'].close()
    config_json.update({"teamdata": result1})
    filen = "activity_app/add_activity.html"
    title = "ACTIVITY LIST"
    return render(request, 'activity_app/base.html',
                  {'add_status': 'active', 'config_json': config_json, 'title': title, 'filen': filen})


@csrf_exempt
def delete_activity(request):
    activity_id = request.POST.get('activity_id')
    try:
        cursor = connections['default'].cursor()
        cursor.execute('DELETE FROM user_activities WHERE activity_id=%s' % activity_id)
        connections['default'].commit()
        connections['default'].close()
        val = "ACTIVITY SUCCESSFULLY DELETED"
    except Exception as exception:
        val = str(exception)
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")


@csrf_exempt
def add_activity_func(request):
    activity = request.POST.get('activity')
    global user_name
    cursor = connections['default'].cursor()
    from datetime import date
    today = str(date.today())
    cursor.execute(
        "select activity_details_id,date,count,user_type from activity_details WHERE user_name='" + user_name +
        "' and date='" + today + "'")
    fieldnames = [name[0] for name in cursor.description]
    print(fieldnames)
    row = cursor.fetchall()
    if not row:
        cursor.execute(
            "select activity_details_id,date,count,user_type from activity_details WHERE user_name='" + user_name + "'")
        row = cursor.fetchall()
    if row[0][2] < 2:
        i = str(row[0][2] + 1)
        # cursor = connections['default'].cursor()
        sql = "insert into  user_activities (user_name, user_type, user_activity) values(%s, %s, %s) "
        cursor.execute(sql, (user_name, row[0][3], activity))
        connections['default'].commit()
        print('UPDATE activity_details SET count=' + i + ',date="' + today + '" WHERE user_name="%s"' % user_name)
        cursor.execute(
            'UPDATE activity_details SET count=' + i + ',date="' + today + '" WHERE user_name="%s"' % user_name)
        connections['default'].commit()
        connections['default'].close()
        val = "added new activity to the list "
    else:
        val = "2 activities only allowed for a day "
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")


@csrf_exempt
def update_result(request):
    id1 = request.POST.get('id')
    user_activity = request.POST.get('user_activity')
    try:
        cursor = connections['default'].cursor()
        print('UPDATE user_activities SET user_activity=' + user_activity + ' WHERE activity_id=%s' % id1)
        cursor.execute('UPDATE user_activities SET user_activity="' + user_activity + '" WHERE activity_id=%s' % id1)
        connections['default'].commit()
        connections['default'].close()
        val = "HEADER SUCCESSFULLY DELETED"
    except Exception as exception:
        val = str(exception)
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")
